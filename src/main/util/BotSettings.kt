package src.main.util

class BotSettings(var apiKey: String = "", var serverAddress: String = "", var serverPort: String = "",
                  var serverPassword: String = "", var channelName: String = "", var channelFilePath: String = "",
                  var nickname: String = "")
